"""
i18nUtils
=====

Module for handling internationalization in Python programs

Copyright (c) 2020 Tom Arn, www.t-arn.com

For suggestions and questions:
<sw@t-arn.com>

This file is distributed under the terms of the MIT license
"""

import i18n
import locale
from pathlib import Path

class i18nUtils:
    version = '0.1'
    version_date = '2020-07-03 - 2020-07-03'
    fallbackLang = None
    lang = None
    translationDir = None

    def __init__(self, translationDir, fallbackLang, lang=None):
        """
        Initializes the class and loads the translation files

        :param translationDir: (str) The path to the directory containing the translation files in the format xx.yml
                               where xx is the language, e.g. en.yml
        :param fallbackLang: (str) The language to be used if the chosen language is not available
        :param lang: (str) The language to use. Defaults to self.getDefaultAppLanguage()
        """
        self.translationDir = translationDir
        self.fallbackLang = fallbackLang
        if lang is None:
            self.lang = self.getDefaultAppLanguage()
        else:
            self.lang = lang
        self.load_i18n(translationDir)
    # __init__

    def getAppLanguages(self):
        """
        Returns a list of languages supported by the app

        :returns: (str[]) The languages for which there are translation files
        """
        _languages = []
        _p = Path(self.translationDir)
        for _child in _p.iterdir():
            if _child.name.endswith('.yml'):
                _languages.append(_child.stem)
        # for
        return _languages
    # getAppLanguages

    @staticmethod
    def getDefaultSystemLanguage():
        """
        Returns the default language of the system
        or 'en' when default language cannot be determined

        :returns: (str) The default language of the system or 'en'
        """
        lang = 'en'
        defaultLocale = locale.getdefaultlocale()
        if defaultLocale is not None:
            lang = defaultLocale[0][0:2]
        return lang
    # getDefaultSystemLanguage

    def getDefaultAppLanguage(self):
        """
        Returns the default language of the app.

        :returns: (str) getDefaultSystemLanguage() if it is
        in getAppLanguages(). Otherwise, it will return self.fallbackLang
        """
        defLang = i18nUtils.getDefaultSystemLanguage()
        if defLang not in self.getAppLanguages():
            defLang = self.fallbackLang
        return defLang
    # getDefaultAppLanguage

    def getErrorTranslation(self, text):
        """
        Returns the translation of the error text or the original error text
        To translate error texts, remove all '.' in the message, prefix it with 'python.error.' and use this as the key, e.g.
        python.error.factorial() not defined for negative values: factorial() ist nicht definiert für negative Werte

        :param text: (str) The error text
        :returns: (str) The translated error text if found. Otherwise the original error text
        """
        _key = text.replace('.', '')
        _key = 'python.error.' + _key
        _trans = self.t(_key)
        if _trans != _key:
            return _trans
        else:
            return text
    # getErrorTranslation

    def load_i18n(self, dirname = ''):
        """
        Loads the translation files from the passed directory or (when not passed) from
        the default translation directory which is in __init__
        The translation files must be named xx.yml where xx is the language code, e.g. en.yml

        :param dirname: (str) The path to the directory with the translation files
        """
        if dirname == '':
            dirname = self.translationDir
        i18n.set('skip_locale_root_data', True)
        i18n.set('filename_format', '{locale}.{format}')
        i18n.set('enable_memoization', True)
        i18n.set('locale', self.lang)
        i18n.set('fallback', self.fallbackLang)
        i18n.load_path.append(dirname)
    # load_i18n

    def t(self, key, count=2):
        """
        Gets the translation for the passed text key
        If the key cannot be found in the set language, the key
        itself will be returned

        :param key: (str) The key for text
        :param count: (int) Pluralization of the text. 0 will choose the 'zero' element, 1 the 'one' element, 2 or greater
                      will choose the 'many' element of the translated text
        :returns: (str) Returns the translated or the key itself when no translation was found
        """
        _text = i18n.t(key, count=count)
        if _text == '':
            _text = key
        if _text.startswith('{') and _text.endswith('}'):
            # fix for BabelEdit not supporting dictionaries and python-i18n not supporting
            # dictionaries formatted as strings
            _dict = eval(_text)
            if count == 0:
                _text = _dict['zero']
            elif count == 1:
                _text = _dict['one']
            else:
                _text = _dict['many']
        return _text
    # t
# i18nUtils
