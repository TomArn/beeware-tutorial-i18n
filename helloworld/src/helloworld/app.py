"""
My first application
"""
import toga
from toga.style import Pack
from toga.style.pack import COLUMN, ROW
import helloworld.i18nUtils
import math # needed for math functions in evaluated expression
import sys

_i18dir = None
_i18n = None

class HelloWorld(toga.App):
    copy_right = 'Freeware 2020, t-arn.com'
    hello_count_so_far = 0

    def startup(self):
        """
        Construct and show the Toga application.

        Usually, you would add your application to a main content box.
        We then create a main window (with a name matching the app), and
        show the main window.
        """
        global _i18dir, _i18n

        _i18dir = str(self.paths.app) + '/resources/i18n'
        print(_i18dir)
        # Normally, you would use
        #   _i18n = helloworld.i18nUtils.I18nUtils(_i18dir, 'en')
        # to choose the language that matches the language of your system or english if there is no translation
        # file for your system language.
        #
        # Or, you could let the user choose a language (e.g. 'de') from
        #   _i18n.get_app_languages()
        # store this language in a config file, load it from there and pass it as follows:
        _i18n = helloworld.i18nUtils.I18nUtils(_i18dir, 'en', 'de')
        print (_i18n.get_app_languages())

        main_box = toga.Box(style=Pack(direction=COLUMN))

        name_label = toga.Label(
            _('app.lblName'),
            style=Pack(padding=(0,5))  # 5px padding on left and right 0px on top and bottom
        )
        self.name_input = toga.TextInput(style=Pack(flex=1))
        
        name_box = toga.Box(style=Pack(direction=ROW, padding=5)) # 5px padding on all sides
        name_box.add(name_label)
        name_box.add(self.name_input)
        
        button = toga.Button(
            _('app.btnHello'),
            on_press=self.say_hello,
            style=Pack(padding=5)
        )
        
        main_box.add(name_box)
        main_box.add(button)

        # Adding some controls to demonstrate the translation of (some) Python errors
        # try a translated one:
        #   math.factorial(-1)
        # or an untranslated one (division by zero)
        #   4/0
        calc_label = toga.Label(
            _('app.lblCalc'),
            style=Pack(padding=(0, 5))  # 5px padding on left and right 0px on top and bottom
        )
        self.calc_input = toga.TextInput(style=Pack(flex=1))
        calc_box = toga.Box(style=Pack(direction=ROW, padding=5))  # 5px padding on all sides
        calc_box.add(calc_label)
        calc_box.add(self.calc_input)
        calc_button = toga.Button(
            _('app.btnCalculate'),
            on_press=self.calculate,
            style=Pack(padding=5)
        )
        main_box.add(calc_box)
        main_box.add(calc_button)

        self.main_window = toga.MainWindow(title=self.formal_name)
        self.main_window.content = main_box

        # adding commands
        self.commands = toga.CommandSet(self.factory) # replaces the default (not localized) CommandSet
        # is there a better way to get rid of the default menu ?
        # File > Preferences, Exit
        # Help > About, Homepage
        grpFile = toga.Group(label=_('app.grpFile'))
        # add actions
        cmdHello = toga.Command(
            self.say_hello,
            label=_('app.cmdHello'),
            group=grpFile
        )
        self.commands.add(cmdHello)
        cmdExit = toga.Command(
            lambda s: self.exit(),
            label=_('app.cmdExit'),
            group=grpFile,
            section=sys.maxsize
        )
        self.commands.add(cmdExit)
        grpHelp = toga.Group(label=_('app.grpHelp'))
        cmdAbout = toga.Command(
            self.handle_about,
            label=_('app.cmdAbout'),
            group=grpHelp
        )
        self.commands.add(cmdAbout)
        self.main_window.show()
        
    def handle_about(self, widget):
        self.main_window.info_dialog(
            _('app.titAbout'),
            _('app.msgAbout').format(self.app.version, self.copy_right)
        )

    def say_hello(self, widget):
        if self.name_input.value:
            name = self.name_input.value
        else:
            name = _('app.msgStranger')
            
        self.main_window.info_dialog(
            _('app.titHello'),
            _('app.msgHello') + ", {}".format(name)
        )

        # demonstration of pluralization:
        # click on the 'Say hello' button several times and see how the message changes
        self.main_window.info_dialog(
            _('app.titHello'),
            _('app.msgCountSoFar', count=self.hello_count_so_far)
        )
        self.hello_count_so_far += 1

    def calculate(self, widget):
        _term = self.calc_input.value
        try:
            _result = eval(_term)
            self.main_window.info_dialog(
                _('app.titResult'),
                _('app.msgResult') + " {}".format(_result)
            )
        except Exception as e:
            self.main_window.info_dialog(
                _('app.titError'),
                _i18n.get_error_translation(str(e))
            )


# method for translations
def _(key, **kwargs):
    return _i18n.t(key, **kwargs)


def main():
    return HelloWorld()
